package dbconfig

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	_ "github.com/newrelic/go-agent/_integrations/nrpq"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func TestConnection(t *testing.T) {
	Convey("Given db configuration to test db connection", t, func() {
		const connStr = "user=%s password=%s dbname=%s host=%s port=%d sslmode=disable"
		DBURL := fmt.Sprintf(connStr,
			"postgres",
			"123456",
			"userapi",
			"localhost",
			5432)
		Convey("When trying to open a new connection", func() {
			Conn, err := sqlx.Open("nrpostgres", DBURL)
			Convey("Then connection should be established and should not throw any error", func() {
				So(err, ShouldBeNil)
				So(Conn, ShouldNotBeNil)
			})
		})
	})
}
