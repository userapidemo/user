package handler

import (
	"bitbucket.org/userapidemo/user/internal/data"
	"bitbucket.org/userapidemo/user/internal/model"
	proto "bitbucket.org/userapidemo/user/pkg/generated/api"
	"context"
	"github.com/jmoiron/sqlx"
)

type handler struct {
	svc data.UserRepo
}

func NewUserService(conn *sqlx.DB) proto.UserServiceHandler {
	return &handler{
		svc: data.NewDataService(conn),
	}
}

func (h *handler) CreateUser(ctx context.Context, req *proto.UserRequest, res *proto.UserResponse) error {
	user := &model.User{Name: req.Name}
	id, err := h.svc.CreateUser(ctx, user)
	if err != nil {
		res.Status = "failed"
		return err
	}

	res.Uid = id
	res.Status = "success"
	return nil
}
