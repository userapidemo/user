package handler

import (
	proto "bitbucket.org/userapidemo/user/pkg/generated/api"
	"context"
	"github.com/asim/go-micro/v3"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func TestCreateUser(t *testing.T) {
	Convey("Given fresh data for test CreateUser", t, func() {
		testStatus := "success"
		testName := "Smith"
		client := micro.NewService()
		service := proto.NewUserService("sf.user.service", client.Client())
		Convey("When calling rpc endpoint CreateUser", func() {
			res, err := service.CreateUser(context.Background(), &proto.UserRequest{
				Name: testName,
			})
			Convey("Then the endpoint should not through any error and response can't be nil", func() {
				So(err, ShouldBeNil)
				So(res, ShouldNotBeNil)
			})
			Convey("Then user id should not be zero", func() {
				So(res.GetUid(), ShouldNotBeZeroValue)
			})
			Convey("Then response status should be \"success\"", func() {
				So(res.Status, ShouldEqual, testStatus)
			})
		})
	})
}

//
//type testCase struct {
//	Id   int32
//	Name string
//}
//
//var userRequest = &proto.UserRequest{}
//var UserResponse = &proto.UserResponse{}
//
//var handler1 = &handler{svc: data.NewDataService(dbconfig.GetConnSQLX())}
//
//func TestUserHandler(t *testing.T) {
//	//user := &model.User{Id: 100, Name: "john"}
//
//	//tests := []struct {
//	//	name string
//	//	want string
//	//}{
//	//	{
//	//		name: "world",
//	//		want: "Hello world",
//	//	},
//	//	{
//	//		name: "123",
//	//		want: "Hello 123",
//	//	},
//	//}
//
//	req := &proto.UserRequest{Id: 1, Name: "john"}
//	res := &proto.UserResponse{}
//
//	//id, err := handler1.svc.CreateUser(context.Background(), user)
//
//	//if err != nil {
//	//	fmt.Println("Got error!!")
//	//} else {
//	//	fmt.Println("success: ", id)
//	//}
//
//	err := handler1.CreateUser(context.Background(), req, res)
//	if err != nil {
//		fmt.Println("err", err)
//	} else {
//		fmt.Println("done.. ")
//	}
//
//}
