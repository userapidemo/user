package data

import (
	"bitbucket.org/userapidemo/user/internal/dbconfig"
	"bitbucket.org/userapidemo/user/internal/model"
	"context"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func TestUserService(t *testing.T) {
	Convey("Given test user", t, func() {
		testUserModel := &model.User{
			Name: "john",
		}
		dataService := NewDataService(dbconfig.GetConnSQLX())
		Convey("When calling method CreateUser()", func() {
			id, err := dataService.CreateUser(context.Background(), testUserModel)
			Convey("Then the method should not throw any error", func() {
				So(err, ShouldBeNil)
			})
			Convey("Then newly created id should not be zero", func() {
				So(id, ShouldNotBeZeroValue)
			})
		})
	})
}
