package data

import (
	"bitbucket.org/userapidemo/user/internal/model"
	"context"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

type UserRepo interface {
	CreateUser(ctx context.Context, request *model.User) (int32, error)
}

type user struct {
	conn *sqlx.DB
}

func NewDataService(db *sqlx.DB) UserRepo {
	return &user{
		conn: db,
	}
}

func (u *user) CreateUser(ctx context.Context, request *model.User) (int32, error) {
	tx := u.conn.MustBegin()
	var id int32
	err := tx.QueryRowx("INSERT INTO user_info.user (name) VALUES ($1) returning id", request.Name).Scan(&id)
	if err != nil {
		tx.Rollback()
		return 0, errors.Wrap(err, "insert address error")
	}
	tx.Commit()
	return id, nil
}
