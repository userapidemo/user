package model

type User struct {
	Id   int32  `db:"id"`
	Name string `db:"name"`
}
