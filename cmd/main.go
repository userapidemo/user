package main

import (
	"bitbucket.org/userapidemo/user/internal/dbconfig"
	"bitbucket.org/userapidemo/user/internal/handler"
	"bitbucket.org/userapidemo/user/pkg/generated/api"
	"github.com/asim/go-micro/v3"
	"github.com/pkg/errors"
	"log"
	"os"
)

func main() {
	service := micro.NewService(
		micro.Name("sf.user.service"),
	)
	service.Init()

	dbconn := dbconfig.GetConnSQLX()
	h := handler.NewUserService(dbconn)

	api.RegisterUserServiceHandler(service.Server(), h)

	if err := service.Run(); err != nil {
		log.Fatalln(nil, errors.Wrap(err, "unable to run service"))
		os.Exit(0)
	}

	os.Exit(0)

}
