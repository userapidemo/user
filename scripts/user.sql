CREATE SCHEMA user_info
CREATE TABLE user_info.user (
                                id         serial  ,
                                name       varchar(20)  ,
                                PRIMARY KEY ( id )
)